# Sonarqube Supply Chain Example

This repo is an example of how to add a new task, a Sonarqube scan, to a TAP SupplyChain. 

It Creates:
- a Tekton Pipeline
- a Tekton Task
- a (templated) Tekton PipelineRun

And it wires those objects to a new Resource in the SupplyChain.

While all the files in this repo _should_ work if you want to use them, the full ClusterSupplyChain file _WILL NOT_. This is because it was copied out of an existing TAP cluster and, thus, has pointers to some values specific to that install (e.g. the URL t the container registry). 

Put more plainly, this repo exists for educational purposes and to provide a starting point for people new to Cartographer.

NOTE: If you want to use this example on your cluster, you will need an existing SonarQube server installed somewhere. The Helm charts are an easy place to start.

![fancy screenshot](tap-gui-screenshot.png)


If you'd like to implement this on your cluster, you'll just need to extract the SupplyChain from your cluster and make edits. As an example, here is how to export the scanning-testing SupplyChain, which is what this repo started with.
```
kubectl get csc source-test-scan-to-url -o yaml > testing-scanning-sonar-csc.yml
```


# How to Read

If you're new to Cartographer and supply chains, it is not immediately obvious what files run in what order. To help, consider reading in the following order.

## [testing-scanning-sonar-csc.yml](testing-scanning-sonar-csc.yml)

This is the top-level file and where the adventure begins. There are minor comments to show what we changed from the original.

## [sonar-clusterSourceTemplate.yml](sonar-clusterSourceTemplate.yml)

In the SupplyChain, we added a new resource, `sonarqube-scan`. That resource has to reference a ClusterTemplate of some sort. Because we need to  emit a source URL, we chose a `ClusterSourceTemplate`. This file is that ClusterSourceTemplate. It contains a templated `PipelineRun`, which is how your app flows through a Tekton Pipeline.

## [tekton-pipeline.yml](tekton-pipeline.yml)

This is the Tekton Pipeline. Largely stolen from this [Cartographer tutorial](https://cartographer.sh/docs/v0.7.0/tutorials/lifecycle/).

## [tekton-task.yml](tekton-task.yml)

This is _THE ACTUAL TASK_. It was borrowed from the Tekton Task Hub and modified to fit our needs (link in the file). Don't worry about the details. Just know that it runs a Sonarqube scan, connecting to an existing Sonarqube server.
